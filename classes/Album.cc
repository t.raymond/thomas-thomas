#include "Album.h"
#include <iostream>
#include <chrono>


Album::Album() :
  id(0),
  //date(std::chrono::system_clock::now()),
  name("RAYGEUR")
{
//  date.std::chrono::system_clock::now();
}

Album::Album(unsigned int _id, std::string _name) :
  id(_id),
  //date(_date),
  name(_name)
{}

Album::~Album() {}

/* std::chrono::system_clock Album::getDate()
{
  return date;
  }*/

unsigned int Album::getId()
{
  return id;
}

std::string Album::getName()
{
  return name;
}

void Album::setId(unsigned _id)
{
  id = _id;
}

void Album::setName(std::string _name)
{
  name = _name;
}

/*
void Album::setDate(std::chrono::system_clock _date)
{
  date = _date;
  }*/
