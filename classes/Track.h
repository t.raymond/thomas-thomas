#ifndef TRACK_H
#define TRACK_H
#include <iostream>
#include <chrono>
#include <vector>
#include "Artist.h"
#include "Album.h"
#include "Polyphony.h"
#include "Format.h"
#include "Subgenre.h"
#include "Genre.h"
#include <libpq-fe.h>


class Track
{

  //attributes :
 private :
  unsigned int id;
  int duration;
  std::string name;
  std::string path;
  std::vector<Artist> theArtist;
  std::vector<Album> anAlbum;
  Album theAlbum;
  Polyphony thePolyphony;
  Format theFormat;
  Subgenre theSubgenre;
  Genre theGenre;
  Artist anArtist;

 public :
  Track();
  Track(unsigned int, int, std::string, std::string, std::vector<Artist>, std::vector<Album>, Polyphony, Format, Subgenre, Genre);
  Track(unsigned int, int, std::string, std::string, Artist, Album, Polyphony, Format, Subgenre, Genre);
  ~Track();

  void setId(unsigned int);
  void setDuration(int);
  void setName(std::string);
  void setPath(std::string);
  void setArtist(std::vector<Artist>);
  void setAlbum(Album);
  void setAlbum(std::vector<Album>);
  void setPolyphony(Polyphony);
  void setFormat(Format);
  void setSubgenre(Subgenre);
  void setGenre(Genre);
  void setArtist(Artist);

  unsigned int getId();
  int getDuration();
  std::string getPath();
  std::string getName();
  std::vector<Artist> getArtist();
  Album getAlbum();
  std::vector<Album> getAnAlbum();
  Genre getGenre();
  Subgenre getSubgenre();
  Polyphony getPolyphony();
  Format getFormat();
  Artist getAnArtist();

  std::vector<Track> addTrack(std::string _name, PGconn *conn);
};
#endif
