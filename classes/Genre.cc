#include "Genre.h"
#include <iostream>

Genre::Genre() :
  id(0),
  type("")
{}

Genre::Genre(unsigned int _id, std::string _type) :
  id(_id),
  type(_type) {}

Genre::~Genre() {};

void Genre::setId(unsigned int _id)
{
  id = _id;
}

void Genre::setType(std::string _type)
{
  type = _type;
}

unsigned int Genre::getId()
{
  return id;
}

std::string Genre::getType()
{
  return type;
}
