#include "Artist.h"
#include <iostream>

Artist::Artist() :
  id(0),
  name("")
{
  
}

Artist::Artist(unsigned int _id, std::string _name) :
  id(_id),
  name(_name)
{
}

Artist::~Artist()
{
}

void Artist::setName(std::string _name)
{
  name = _name;
}

void Artist::setId(unsigned int _id)
{
  id = _id;
}

std::string Artist::getName()
{
  return name;
}

unsigned int Artist::getId()
{
  return id;
}
