#include <libpq-fe.h>
#include <iostream>
#include <cstring>
#include <cstdlib>
#include <gflags/gflags.h>
#include <chrono>
#include "../classes/Playlist.h"

//using namespace std;

DEFINE_uint64(duration, 60, "duration of playlist");
DEFINE_string(genre, "", "genre of track,quantity");
DEFINE_bool(m3u,false,"write m3u file if true, by default it's true if have not xspf option");
DEFINE_bool(xspf,false,"write xspf file if true, by default it's false");
DEFINE_string(subgenre, "", "subgenre of track,quantity");
DEFINE_string(artist, "", "artist of track,quantity");
DEFINE_string(album, "", "album,quantity");
DEFINE_string(title, "", "title,quantity");
DEFINE_string(name, "Playlist", "name of the playlist and file");

int main(int argc, char **agrv)
{
  ::google::ParseCommandLineFlags(&argc, &agrv, true);
  char con_info[] = "host=postgresql.bts-malraux72.net port=5432 user=t.lehugeur password=P@ssword dbname=Cours";//informations de connexion au$

  int seconds = 0;
  
  PGconn *connexion = PQconnectdb(con_info);//connexion à la bdd

  std::chrono::duration<int> leTemps;
  Track newTrack = Track();
  std::vector<Track> lesTracks = newTrack.addTrack(FLAGS_title, connexion);//selection des morceaux
  
  for(Track t : lesTracks)
  {
    seconds += t.getDuration();
  }
 
  Playlist laPlaylist = Playlist(3, FLAGS_name, leTemps, lesTracks);

  //write options
  if(FLAGS_m3u){
    laPlaylist.writeM3U();
  }
  else if(!FLAGS_xspf && !FLAGS_m3u){
    laPlaylist.writeM3U();
    std::cout << "/!\\ Any option of write are selected, by default m3u file is written" << std::endl;
  }
  if(FLAGS_xspf){
    laPlaylist.writeXSPF();
  }
  return 0;
}
